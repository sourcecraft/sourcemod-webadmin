# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The [Sourcemod-Webadmin](https://forums.alliedmods.net/showthread.php?t=60174) is a webinterface to manage sourcemod plugin's with MySQL Support.
* Version 2.1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
----- How to Install -----

If you allready use the MySQL-Admin Plugin with a database,
please begin with step 4!!!

1.  Create a new mysql database. Ex:admin
2.  Import "create_admins" from "addons/sourcemod/configs/sql-init-scripts/mysql" to the database
3.  Edit "databases.cfg" in "addons/sourcemod/configs" on your gameserver.
4.  Upload all files to your web server to a folder of your choice.
5.  Open the file "../inc/config.php" and change the data to your MySQL Database.
6.  Run the "../setup.php" in your Web browser.
7.  Set the "../temp" directory to "Chmod 777".
8.  Set the "../inc/pics/games" directory to "Chmod 777".
9.  Delete the file "../setup.php" on your web server!
10.  Open the Web interface "../index.php" on your Web browser.
11. Login with...
    Username: admin
    Password: 123456

12. Ready...!

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact